import './App.css';
import { useState } from 'react';
import WeatherCard from './components/WeatherCard';
import { fetchWeather } from './api';
function App() {
  const [city, setCity] = useState('');
  const [weather, setWeather] = useState(null);
  const [error, setError] = useState('');

  const handleChange = (e) => {
    setCity(e.target.value)
  }

  const handleSubmit = async (e) => {
    e.preventDefault();
    try {
      const weather = await fetchWeather(city, setError);
      console.log(weather);
      setWeather(weather)
    } catch (err) {
      setError('City not found')
    }
  }

  return (
    <div className="App">

      <h1 className='app_heading'>Weather App</h1>

      <form className='search_form' onSubmit={handleSubmit}>
        <input className='weather_input' type='text' placeholder='Enter City' value={city} onChange={handleChange} />
        <button className='search_submit' type='submit'>Search</button>
      </form>

      {error ? (
        <p className='error'>{error}</p>
      ) : (
        <WeatherCard weather={weather} />
      )}
    </div>

  );
}

export default App;
