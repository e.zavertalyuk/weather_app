import axios from 'axios'

export async function fetchWeather(city, setError) {
   const apiKey = "8c124acd7eeaa81d097c6cf98ff27b86";
   const url = `https://api.openweathermap.org/data/2.5/weather?q=${city}&appid=${apiKey}`;
   try {
      const response = await axios.get(url);
      setError('');
      return response.data;
   } catch (error) {
      setError('City Not Found!');
      return error;
   }
}
