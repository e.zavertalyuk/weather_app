
const WeatherCard = ({ weather }) => {
   return (
      <div className="weather-card">
         <div className="card-wrap">
            <div className="card-header">
               <div className="weather-wrap">
                  <p className="weather-city">
                     {weather ? (weather.name + " " + weather.sys.country) : "-"}
                  </p>
                  <p className="weather-description">
                     {weather ? (weather.weather[0].description) : "-"}
                  </p>
               </div>
               <img className={'weather-icon'} src={`${weather ? ('weather') : 'unknown'}.png`} width={100} height={100} />
            </div>
            <div className="card-body">
               <h2 className="weather-temp">
                  {weather ? Math.round(weather.main?.temp - 273.15) : ''}ºC
               </h2>
               <div className="weather-details">
                  <div className="parameters">
                     <span className="param-label">Details:</span>
                  </div>
                  <div className="parameters">
                     <span className="param-label">Feels like: </span>
                     <span className="param-val">
                        {weather ? (weather.main?.feels_like - 273.15).toFixed(1) : '-'} C
                     </span>
                  </div>
                  <div className="parameters">
                     <span className="param-label">Humidity: </span>
                     <span className="param-val">
                        {weather ? (weather.main?.humidity) : "-"} %
                     </span>
                  </div>
                  <div className="parameters">
                     <span className="param-label">Pressure: </span>
                     <span className="param-val">
                        {weather ? (weather.main?.pressure) : "-"} hPs
                     </span>
                  </div>
               </div>
            </div>
         </div>
      </div>
   )
}
export default WeatherCard